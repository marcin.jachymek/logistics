package com.example.demo.messages;

import org.springframework.stereotype.Service;

@Service
public class MessagingServiceMock implements MessagingService {

    public void violationMessage(String message) {
        System.out.println(message);
    }
}
