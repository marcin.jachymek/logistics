package com.example.demo.driver.info;

import com.example.demo.converters.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDateTime;

public class DriverInfo {

    private String id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean tourEnd;

    public DriverInfo() {
    }

    public DriverInfo(String id, LocalDateTime startDate, LocalDateTime endDate, boolean tourEnd) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tourEnd = tourEnd;
    }

    public String getId() {
        return id;
    }

    public DriverInfo setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public DriverInfo setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public DriverInfo setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public boolean isTourEnd() {
        return tourEnd;
    }

    public DriverInfo setTourEnd(boolean tourEnd) {
        this.tourEnd = tourEnd;
        return this;
    }
}
