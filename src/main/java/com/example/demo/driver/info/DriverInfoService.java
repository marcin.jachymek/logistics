package com.example.demo.driver.info;

import java.io.IOException;
import java.time.Duration;

public interface DriverInfoService {
    Duration checkDriver(String id) throws NoSuchDriverException, IOException;
}
