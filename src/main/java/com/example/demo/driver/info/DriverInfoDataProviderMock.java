package com.example.demo.driver.info;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class DriverInfoDataProviderMock {
    private static final Map<String, String> driverInfos = new HashMap<>();
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public void setDriverInfos(String id, String driverInfoJson) {
        driverInfos.put(id, driverInfoJson);
    }

    public DriverInfo getDriverInfosId(String driverId) throws NoSuchDriverException, IOException {
        try {
            return objectMapper.readValue(driverInfos.get(driverId), DriverInfo.class);
        } catch (IllegalArgumentException e) {
            throw new NoSuchDriverException("No driver with id: " + driverId);
        }
    }
}
