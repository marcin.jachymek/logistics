package com.example.demo.driver.info;


import com.example.demo.messages.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class DriverInfoServiceImpl implements DriverInfoService {

    public static int WORK_TIME = 8;
    private final MessagingService messagingService;
    private final DriverInfoDataProviderMock driverInfoDataProviderMock;

    @Autowired
    public DriverInfoServiceImpl(MessagingService messagingService, DriverInfoDataProviderMock driverInfoDataProviderMock) {
        this.messagingService = messagingService;
        this.driverInfoDataProviderMock = driverInfoDataProviderMock;
    }

    public Duration checkDriver(String id) throws NoSuchDriverException, IOException {
        DriverInfo driverInfo = getDriverInfo(id);
        return checkDriveTimeDurationInfo(driverInfo);
    }

    private DriverInfo getDriverInfo(String id) throws NoSuchDriverException, IOException {
        return driverInfoDataProviderMock.getDriverInfosId(id);
    }

    private Duration checkDriveTimeDurationInfo(DriverInfo driverInfo) {
        Duration duration = Duration.between(driverInfo.getStartDate(),
                driverInfo.isTourEnd() ? driverInfo.getEndDate() : LocalDateTime.now());
        if (duration.toHours() > WORK_TIME) {
            sendTheTimeViolationMessage(driverInfo, duration);
        }
        return duration;
    }

    private void sendTheTimeViolationMessage(DriverInfo di, Duration duration) {
        String fs = String.format("The driver with id %s, is driving now for %s hours",
                di.getId(), duration.toHours());
        messagingService.violationMessage(fs);
    }

}
