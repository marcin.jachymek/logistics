package com.example.demo.driver.info;

public class NoSuchDriverException extends Throwable {

    public NoSuchDriverException(String errorMessage) {
        super(errorMessage);
    }
}
