package com.example.demo.converters;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {

        try {
            return LocalDateTime.ofInstant(Instant.ofEpochSecond(jsonparser.getLongValue()), ZoneId.systemDefault());
        } catch (JsonParseException exception) {
        }

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd HH:mm:ss");
            return formatter.parse(jsonparser.getText(), LocalDateTime::from);
        } catch (DateTimeParseException e) {
            throw new IOException("DateTime format for date: " + jsonparser.getText() + " not yet supported");
        }

    }
}
