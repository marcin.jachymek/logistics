package com.example.demo;

import com.example.demo.driver.info.DriverInfoDataProviderMock;
import com.example.demo.driver.info.DriverInfoService;
import com.example.demo.driver.info.DriverInfoServiceImpl;
import com.example.demo.driver.info.NoSuchDriverException;
import com.example.demo.messages.MessagingServiceMock;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Duration;

import static com.example.demo.driver.info.DriverInfoServiceImpl.WORK_TIME;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DriverInfoServiceTest {

    DriverInfoDataProviderMock driverInfoDataProviderMock = new DriverInfoDataProviderMock();
    DriverInfoService driverInfoService = new DriverInfoServiceImpl(new MessagingServiceMock(), driverInfoDataProviderMock);

    @Test
    public void checkVeryLongEndedTravel() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("d39bbcf0-e0de-4681-8264-de1f3bbeb12f",
                "{\n" +
                        "   \"id\": \"d39bbcf0-e0de-4681-8264-de1f3bbeb12f\",\n" +
                        "   \"startDate\":1400147200,\n" + // May 15, 2014 9:46:40 AM
                        "   \"endDate\":1570147200,\n" + // October 4, 2019 12:00:00 AM
                        "   \"tourEnd\": true\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("d39bbcf0-e0de-4681-8264-de1f3bbeb12f");
        //then
        assertTrue(drivingDuration.toHours() > WORK_TIME);
    }

    @Test
    public void checkVeryLongNotEndedTravelTimestamp() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("312cf31c-a461-4fbf-97fe-0c0ffe0586fa",
                "{\n" +
                        "   \"id\":\"312cf31c-a461-4fbf-97fe-0c0ffe0586fa\",\n" +
                        "   \"startDate\":1400147200,\n" +
                        "   \"tourEnd\":false\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("312cf31c-a461-4fbf-97fe-0c0ffe0586fa");
        //then
        assertTrue(drivingDuration.toHours() > WORK_TIME);
    }

    @Test
    public void checkVeryLongNotEndedTravelDateTimeFormat() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("c0ad3899-9e35-4cf1-b964-60f7ac9b0a8f",
                "{\n" +
                        "    \"id\":\"c0ad3899-9e35-4cf1-b964-60f7ac9b0a8f\",\n" +
                        "    \"startDate\":\"2020-03-02 12:00:00\",\n" +
                        "    \"tourEnd\":false\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("c0ad3899-9e35-4cf1-b964-60f7ac9b0a8f");
        //then
        assertTrue(drivingDuration.toHours() > WORK_TIME);
    }

    @Test
    public void checkVeryLongEndedTravelDateTimeFormat() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("e5e0228e-df73-4b31-9c2a-47dd9c5102ce",
                "{\n" +
                        "    \"id\":\"e5e0228e-df73-4b31-9c2a-47dd9c5102ce\",\n" +
                        "    \"startDate\":\"2020-03-02 12:00:00\",\n" +
                        "    \"endDate\":\"2020-03-02 23:00:00\",\n" +
                        "    \"tourEnd\":true\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("e5e0228e-df73-4b31-9c2a-47dd9c5102ce");
        //then
        assertTrue(drivingDuration.toHours() > WORK_TIME);
    }

    @Test
    public void checkShortEndedTravelTimestamp() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("d39bbcf0-e0de-4681-8264-de1f3bbeb12y",
                "{\n" +
                        "   \"id\": \"d39bbcf0-e0de-4681-8264-de1f3bbeb12y\",\n" +
                        "   \"startDate\":1400147200,\n" + // May 15, 2014 9:46:40 AM
                        "   \"endDate\":1400167200,\n" +
                        "   \"tourEnd\": true\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("d39bbcf0-e0de-4681-8264-de1f3bbeb12y");
        //then
        assertTrue(drivingDuration.toHours() < WORK_TIME);
    }

    @Test
    public void checkShortEndedTravelDateTimeFormat() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("e5e0228e-df73-4b31-9c2a-47dd9c5102ca",
                "{\n" +
                        "    \"id\":\"e5e0228e-df73-4b31-9c2a-47dd9c5102ca\",\n" +
                        "    \"startDate\":\"2020-03-02 12:00:00\",\n" +
                        "    \"endDate\":\"2020-03-02 13:00:00\",\n" +
                        "    \"tourEnd\":true\n" +
                        "}");
        //when
        Duration drivingDuration = driverInfoService.checkDriver("e5e0228e-df73-4b31-9c2a-47dd9c5102ca");
        //then
        assertTrue(drivingDuration.toHours() < WORK_TIME);
    }

    @Test
    public void checkForNonExistingDriver() {
        //given
        driverInfoDataProviderMock.setDriverInfos("e5e0228e-df73-4b31-9c2a-47dd9c5102ce",
                "{\n" +
                        "    \"id\":\"e5e0228e-df73-4b31-9c2a-47dd9c5102ce\",\n" +
                        "    \"startDate\":\"2020-03-02 12:00:00\",\n" +
                        "    \"endDate\":\"2020-03-02 13:00:00\",\n" +
                        "    \"tourEnd\":true\n" +
                        "}");
        //then
        NoSuchDriverException exception = assertThrows(NoSuchDriverException.class, () -> {
            driverInfoService.checkDriver("e5e0228e-df73-4b31-9c2a-47dd9c510000");
        });

        String expectedMessage = "No driver with id: e5e0228e-df73-4b31-9c2a-47dd9c510000";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void checkForUnsupportedDateFormat() throws NoSuchDriverException, IOException {
        //given
        driverInfoDataProviderMock.setDriverInfos("e5e0228e-df73-4b31-9c2a-47dd9c5102c1",
                "{\n" +
                        "    \"id\":\"e5e0228e-df73-4b31-9c2a-47dd9c5102c1\",\n" +
                        "    \"startDate\":\"2020/03/02 12:00:00\",\n" +
                        "    \"tourEnd\":true\n" +
                        "}");
        //then
        IOException exception = assertThrows(IOException.class, () -> {
            driverInfoService.checkDriver("e5e0228e-df73-4b31-9c2a-47dd9c5102c1");
        });

        String expectedMessage = "DateTime format for date: 2020/03/02 12:00:00 not yet supported";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}

